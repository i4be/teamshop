<?php
    
    namespace Drupal\commerce_marketplace_view_own_stores\EventSubscriber;
    
    use Drupal\entity\QueryAccess\ConditionGroup;
    use Drupal\entity\QueryAccess\QueryAccessEvent;
    use Symfony\Component\EventDispatcher\EventSubscriberInterface;
    
    class QueryAccessSubscriber implements EventSubscriberInterface {
    
      /**
       * {@inheritdoc}
       */
      public static function getSubscribedEvents() {
        return [
          'entity.query_access.commerce_store' => 'onQueryAccess',
        ];
      }
    
      /**
       * Adds conditions for the "view own" permission.
       *
       * @param \Drupal\entity\QueryAccess\QueryAccessEvent $event
       *   The event.
       */
      public function onQueryAccess(QueryAccessEvent $event) {
        $conditions = $event->getConditions();
        $operation = $event->getOperation();
        $account = $event->getAccount();
        print "hello";
        if ($operation != 'view') {
          return;
        }
        // The user already has full access due to a
        // "administer commerce_store" or "view commerce_store"
        // permission.
        if (!$conditions->count() && !$conditions->isAlwaysFalse()) {
        //   return;
        }
    
        // Or skip the permission check completely if you want
        // to always restrict the user to viewing their own store.
        if ($account->hasPermission('view own commerce_store')) {
          $conditions->addCacheContexts(['user']);
          $conditions->addCondition('uid', $account->id());
          $conditions->alwaysFalse(FALSE);
        }
      }
    
    }