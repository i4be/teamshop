#drupal_commerce2.*

Integration instructions:
 you will need a working Drupal with the latest version of Drupal Commerce installed.

1. Unzip and copy the payfast folder to the ../modules/contrib/commerce/modules directory
2. Log into the admin dashboard and install Commerce PayFast on the 'Extend' page.
3. Navigate to Commerce>Configuration>Payments and click on 'new payment gateway'
4. Select PayFast and configure as required.
5. Click Save.
